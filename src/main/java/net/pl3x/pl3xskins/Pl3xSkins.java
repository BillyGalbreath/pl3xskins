package net.pl3x.pl3xskins;

import java.io.IOException;
import java.util.logging.Level;

import net.pl3x.pl3xskins.commands.*;
import net.pl3x.pl3xskins.configuration.PlayerConfigManager;
import net.pl3x.pl3xskins.listeners.*;

import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.keyboard.Keyboard;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.mcstats.Metrics;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xSkins extends JavaPlugin {
	public void onEnable() {
		loadConfig();
		
		PluginManager pm = Bukkit.getPluginManager();
		
		if (pm.getPlugin("Spout") == null || !pm.isPluginEnabled("Spout")) {
			log(ChatColor.RED + "SpoutPlugin is required for this plugin to function.");
			log(ChatColor.RED + "Pl3xSkins is disabling itself due to lack of dependency plugins.");
			pm.disablePlugin(this);
			return;
		}
		
		pm.registerEvents(new PlayerListener(this), this);
		
		Keyboard key = Keyboard.valueOf("KEY_" + getConfig().getString("gui-hotkey"));
		if (key == null) {
			log(ChatColor.RED + "GUI Hotkey not found, overriding with default setting F7.");
			key = Keyboard.KEY_F7;
		}
		SpoutManager.getKeyBindingManager().registerBinding("Pl3xSkins_GUI", key, "Opens the Skins/Capes GUI", new KeyListener(this), this);
		
		if (getConfig().getInt("skin-refresh-rate") < 1) {
			log(ChatColor.RED + "Skin Refresh Rate set too low, overriding with safe default setting of 3.");
			getConfig().set("skin-refresh-rate", 3);
		}
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() { loadAllPlayerSkinsCapes(); }
		}, ((long) getConfig().getInt("skin-refresh-rate") * 20),
		(long) getConfig().getInt("skin-refresh-rate") * 20);
		
		this.getCommand("pl3xskins").setExecutor(new CmdPl3xSkins(this));
		this.getCommand("skin").setExecutor(new CmdSkin(this));
		this.getCommand("cape").setExecutor(new CmdCape(this));
		
		setupMetrics();
		
		log("Pl3xCraft v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		PlayerConfigManager.saveAllManagers();
		PlayerConfigManager.removeAllManagers();
		log("Pl3xCraft Disabled.");
	}
	
	public void loadConfig() {
		log("Loading main configuration file.");
		if (!getConfig().contains("debug-mode")) getConfig().addDefault("debug-mode", "false");
		if (!getConfig().contains("color-logs")) getConfig().addDefault("color-logs", "true");
		if (!getConfig().contains("gui-hotkey")) getConfig().addDefault("gui-hotkey", "F7");
		if (!getConfig().contains("skin-refresh-rate")) getConfig().addDefault("skin-refresh-rate", 5);
		if (!getConfig().contains("skin-background-url")) getConfig().addDefault("skin-background-url", "https://dl.dropboxusercontent.com/u/93303130/skin-background.png");
		if (!getConfig().contains("cape-background-url")) getConfig().addDefault("cape-background-url", "https://dl.dropboxusercontent.com/u/93303130/cape-background.png");
		if (!getConfig().contains("preview-url")) getConfig().addDefault("preview-url", "http://wiki.pl3x.net/skin/skin.php");
		if (!getConfig().contains("scale-url")) getConfig().addDefault("scale-url", "http://wiki.pl3x.net/skin/cape.php");
		getConfig().options().copyDefaults(true);
		saveConfig();
		log("Loaded main configuration file.");
	}
	
	public void log (final Object obj) {
		if (getConfig().getBoolean("color-logs")) {
			getServer().getConsoleSender().sendMessage(
					ChatColor.AQUA + "[" + ChatColor.LIGHT_PURPLE + getName() + ChatColor.AQUA + "] " + ChatColor.RESET + obj
			);
		} else {
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
		}
	}
	
	public void loadAllPlayerSkinsCapes() {
		for (Player player : getServer().getOnlinePlayers()) {
			if (((SpoutPlayer) player).isSpoutCraftEnabled()) {
				PlayerConfigManager pcm = PlayerConfigManager.getManager(player.getName());
				String skin = pcm.getString("custom-skin");
				String cape = pcm.getString("custom-cape");
				Boolean doScale = pcm.getBoolean("scale-cape");
				try {
					if (skin != null && !skin.equals("") && !skin.equals("http://"))
						((SpoutPlayer) player).setSkin(skin);
					if (cape != null && !cape.equals("") && !cape.equals("http://"))
						if (doScale)
							cape = getConfig().getString("scale-url", "http://wiki.pl3x.net/skin/cape.php") + "?scale=true&url=" + cape + "&.png";
						((SpoutPlayer) player).setCape(cape);
				} catch(Exception e) { /* DO NOTHING */}
			}
		}
	}
	
	private void setupMetrics() {
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}
	}
}
