package net.pl3x.pl3xskins.guis;

import net.pl3x.pl3xskins.Pl3xSkins;
import net.pl3x.pl3xskins.configuration.PlayerConfigManager;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.getspout.spoutapi.event.screen.TextFieldChangeEvent;
import org.getspout.spoutapi.gui.GenericButton;
import org.getspout.spoutapi.gui.GenericCheckBox;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.GenericPopup;
import org.getspout.spoutapi.gui.GenericTextField;
import org.getspout.spoutapi.gui.GenericTexture;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.WidgetAnchor;
import org.getspout.spoutapi.player.SpoutPlayer;

public class CapeSelectScreen extends GenericPopup {
	public Pl3xSkins jp;
	public GenericTextField capeUrlField;
	public GenericTexture previewImage;
	public String previewUrl;
	public Boolean doScale = false;
	
	public CapeSelectScreen(Pl3xSkins plugin, Player player) {
		jp = plugin;
		PlayerConfigManager pcm = PlayerConfigManager.getManager(player.getName());
		String skinurl = pcm.getString("custom-skin");
		if (emptyUrl(skinurl))
			skinurl = getDefaultSkin(((SpoutPlayer) player).getName());
		String backgroundUrl = plugin.getConfig().getString("cape-background-url", "https://dl.dropboxusercontent.com/u/93303130/cape-background.png");
		previewUrl = plugin.getConfig().getString("preview-url", "http://wiki.pl3x.net/skin/skin.php");
		String capeurl = pcm.getString("custom-cape");
		doScale = pcm.getBoolean("scale-cape");
		
		GenericTexture background = new GenericTexture();
		background.setAnchor(WidgetAnchor.CENTER_CENTER);
		background.setUrl(backgroundUrl);
		background.setX(-200).setY(-84);
		background.setWidth(400).setHeight(168);
		background.setPriority(RenderPriority.Highest);
		
		previewImage = new GenericTexture();
		previewImage.setAnchor(WidgetAnchor.CENTER_CENTER);
		if (!emptyUrl(capeurl))
			previewImage.setUrl(getPreviewCape(skinurl, capeurl));
		else
			previewImage.setUrl(getPreviewCape(skinurl, null));
		previewImage.setX(background.getX() + 312).setY(background.getY() + 32);
		previewImage.setWidth(60).setHeight(120);
		previewImage.setPriority(RenderPriority.Lowest);
		
		GenericLabel capeurllabel = new GenericLabel();
		capeurllabel.setAnchor(WidgetAnchor.CENTER_CENTER);
		capeurllabel.setX(background.getX() + 10).setY(background.getY() + 35);
		capeurllabel.setWidth(200).setHeight(20);
		capeurllabel.setPriority(RenderPriority.High);
		capeurllabel.setText("Cape URL:");
		
		capeUrlField = new GenericTextField() {
			@Override
			public void onTextFieldChange(TextFieldChangeEvent event) {
				String capeurl = this.getText();
				String skinurl = event.getPlayer().getSkin();
				if (emptyUrl(skinurl))
					skinurl = getDefaultSkin(event.getPlayer().getName());
				if (!emptyUrl(capeurl))
					previewImage.setUrl(getPreviewCape(skinurl, capeurl));
				else
					previewImage.setUrl(getPreviewCape(skinurl, null));
			}
		};
		capeUrlField.setAnchor(WidgetAnchor.CENTER_CENTER);
		capeUrlField.setX(background.getX() + 10).setY(background.getY() + 45);
		capeUrlField.setWidth(275).setHeight(75);
		capeUrlField.setPriority(RenderPriority.Normal);
		capeUrlField.setMaximumCharacters(300);
		capeUrlField.setMaximumLines(6);
		capeUrlField.setFixed(true);
		if (capeurl == null || capeurl.equals("") || capeurl.equals("http://")) {
			capeUrlField.setText("http://");
		} else {
			capeUrlField.setText(capeurl);
		}
		
		GenericButton save = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				SpoutPlayer player = event.getPlayer();
				String capeurl = capeUrlField.getText();
				if (setCape(player, capeurl))
					event.getPlayer().getMainScreen().getActivePopup().close();
			}
		};
		save.setAnchor(WidgetAnchor.CENTER_CENTER);
		save.setX(background.getX() + 15).setY(background.getY() + 135);
		save.setWidth(50).setHeight(20);
		save.setPriority(RenderPriority.Low);
		save.setText("Save");
		
		GenericButton reset = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				String skinurl = event.getPlayer().getSkin();
				if (emptyUrl(skinurl))
					skinurl = getDefaultSkin(event.getPlayer().getName());
				capeUrlField.setText("http://");
				previewImage.setUrl(getPreviewCape(skinurl, null));
			}
		};
		reset.setAnchor(WidgetAnchor.CENTER_CENTER);
		reset.setX(background.getX() + 75).setY(background.getY() + 135);
		reset.setWidth(50).setHeight(20);
		reset.setPriority(RenderPriority.Low);
		reset.setText("Reset");
		
		GenericCheckBox scale = new GenericCheckBox() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				doScale = this.isChecked();
				String capeurl = capeUrlField.getText();
				String skinurl = event.getPlayer().getSkin();
				if (emptyUrl(skinurl))
					skinurl = getDefaultSkin(event.getPlayer().getName());
				if (!emptyUrl(capeurl)) {
					previewImage.setUrl(getPreviewCape(skinurl, capeurl));
					return;
				}
				previewImage.setUrl(getPreviewCape(skinurl, null));
			}
		};
		scale.setAnchor(WidgetAnchor.CENTER_CENTER);
		scale.setX(background.getX() + 170).setY(background.getY() + 135);
		scale.setWidth(50).setHeight(20);
		scale.setPriority(RenderPriority.Low);
		scale.setText("Scale");
		scale.setChecked(doScale);
		
		GenericButton preview = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				String capeurl = capeUrlField.getText();
				String skinurl = event.getPlayer().getSkin();
				if (emptyUrl(skinurl))
					skinurl = getDefaultSkin(event.getPlayer().getName());
				if (!emptyUrl(capeurl)) {
					previewImage.setUrl(getPreviewCape(skinurl, capeurl));
					return;
				}
				previewImage.setUrl(getPreviewCape(skinurl, null));
			}
		};
		preview.setAnchor(WidgetAnchor.CENTER_CENTER);
		preview.setX(background.getX() + 230).setY(background.getY() + 135);
		preview.setWidth(50).setHeight(20);
		preview.setPriority(RenderPriority.Low);
		preview.setText("Preview");
		
		this.setTransparent(false);
		this.setFixed(true);
		this.attachWidgets(plugin, background, previewImage, capeurllabel, capeUrlField, save, reset, scale, preview);
	}
	
	private boolean emptyUrl(String url) {
		if (	url == null ||
				url.equals("") ||
				url.equals("http://") ||
				url.equals("https://") ||
				url.equals("ftp://")) {
			return true;
		} else {
			return false;
		}
	}
	
	private String getDefaultSkin(String name) {
		return "http://s3.amazonaws.com/MinecraftSkins/" + name + ".png";
	}
	
	private String getPreviewCape(String skinurl, String capeurl) {
		return previewUrl + "?back=true" + (doScale ? "&scale=true" : "") + "&url=" + skinurl + "&cape=" + capeurl + "&.png";
	}
	
	private boolean setCape(SpoutPlayer player, String capeurl) {
		PlayerConfigManager pcm = PlayerConfigManager.getManager(player.getName());
		if (emptyUrl(capeurl)) {
			pcm.set("custom-cape", null);
			pcm.set("scale-cape", null);
			pcm.forceSave();
			player.resetCape();
			return true;
		}
		pcm.set("custom-cape", capeurl);
		pcm.set("scale-cape", doScale);
		pcm.forceSave();
		try {
			if (doScale)
				capeurl = jp.getConfig().getString("scale-url", "http://wiki.pl3x.net/skin/cape.php") + "?scale=true&url=" + capeurl + "&.png";
			player.setCape(capeurl);
		} catch (UnsupportedOperationException e) {
			String tryCape = capeurl + "?.png";
			if (capeurl.contains("?"))
				tryCape = capeurl + "&.png";
			try {
				player.setCape(tryCape);
			} catch (UnsupportedOperationException e2) {
				sendNotification(player, "ERROR!", "Unable to load URL!", 377);
				return false;
			}
		}
		return true;
	}
	
	private void sendNotification(SpoutPlayer player, String label, String message, int materialId) {
		player.sendNotification(label, message, Material.getMaterial(materialId));
	}
}
