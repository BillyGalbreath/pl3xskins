package net.pl3x.pl3xskins.guis;

import net.pl3x.pl3xskins.Pl3xSkins;
import net.pl3x.pl3xskins.configuration.PlayerConfigManager;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.getspout.spoutapi.event.screen.TextFieldChangeEvent;
import org.getspout.spoutapi.gui.GenericButton;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.GenericPopup;
import org.getspout.spoutapi.gui.GenericTextField;
import org.getspout.spoutapi.gui.GenericTexture;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.WidgetAnchor;
import org.getspout.spoutapi.player.SpoutPlayer;

public class SkinSelectScreen extends GenericPopup {
	public Pl3xSkins jp;
	public GenericTextField skinUrlField;
	public GenericTexture previewImage;
	public String previewUrl;
	
	public SkinSelectScreen(Pl3xSkins plugin, Player player) {
		jp = plugin;
		PlayerConfigManager pcm = PlayerConfigManager.getManager(player.getName());
		String backgroundUrl = plugin.getConfig().getString("skin-background-url", "https://dl.dropboxusercontent.com/u/93303130/skin-background.png");
		previewUrl = plugin.getConfig().getString("preview-url", "http://wiki.pl3x.net/skin/skin.php");
		String url = pcm.getString("custom-skin");
		
		GenericTexture background = new GenericTexture();
		background.setAnchor(WidgetAnchor.CENTER_CENTER);
		background.setUrl(backgroundUrl);
		background.setX(-200).setY(-84);
		background.setWidth(400).setHeight(168);
		background.setPriority(RenderPriority.Highest);
		
		previewImage = new GenericTexture();
		previewImage.setAnchor(WidgetAnchor.CENTER_CENTER);
		if (!emptyUrl(url))
			previewImage.setUrl(getPreviewSkin(url));
		else
			previewImage.setUrl(getDefaultSkin(player.getName()));
		previewImage.setX(background.getX() + 312).setY(background.getY() + 32);
		previewImage.setWidth(60).setHeight(120);
		previewImage.setPriority(RenderPriority.Lowest);
		
		GenericLabel skinurllabel = new GenericLabel();
		skinurllabel.setAnchor(WidgetAnchor.CENTER_CENTER);
		skinurllabel.setX(background.getX() + 10).setY(background.getY() + 35);
		skinurllabel.setWidth(200).setHeight(20);
		skinurllabel.setPriority(RenderPriority.High);
		skinurllabel.setText("Skin URL:");
		
		skinUrlField = new GenericTextField() {
			@Override
			public void onTextFieldChange(TextFieldChangeEvent event) {
				String name = event.getPlayer().getName();
				String skinurl = this.getText();
				if (!emptyUrl(skinurl))
					previewImage.setUrl(getPreviewSkin(skinurl));
				else
					previewImage.setUrl(getDefaultSkin(name));
			}
		};
		skinUrlField.setAnchor(WidgetAnchor.CENTER_CENTER);
		skinUrlField.setX(background.getX() + 10).setY(background.getY() + 45);
		skinUrlField.setWidth(275).setHeight(75);
		skinUrlField.setPriority(RenderPriority.Normal);
		skinUrlField.setMaximumCharacters(300);
		skinUrlField.setMaximumLines(6);
		skinUrlField.setFixed(true);
		if (url == null || url.equals("") || url.equals("http://")) {
			skinUrlField.setText("http://");
		} else {
			skinUrlField.setText(url);
		}
		
		GenericButton save = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				SpoutPlayer player = event.getPlayer();
				String skinurl = skinUrlField.getText();
				if (setSkin(player, skinurl))
					event.getPlayer().getMainScreen().getActivePopup().close();
			}
		};
		save.setAnchor(WidgetAnchor.CENTER_CENTER);
		save.setX(background.getX() + 15).setY(background.getY() + 135);
		save.setWidth(50).setHeight(20);
		save.setPriority(RenderPriority.Low);
		save.setText("Save");
		
		GenericButton reset = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				String name = event.getPlayer().getName();
				skinUrlField.setText("http://");
				previewImage.setUrl(getDefaultSkin(name));
			}
		};
		reset.setAnchor(WidgetAnchor.CENTER_CENTER);
		reset.setX(background.getX() + 75).setY(background.getY() + 135);
		reset.setWidth(50).setHeight(20);
		reset.setPriority(RenderPriority.Low);
		reset.setText("Reset");
		
		GenericButton preview = new GenericButton() {
			@Override
			public void onButtonClick(ButtonClickEvent event) {
				String name = event.getPlayer().getName();
				String skinurl = skinUrlField.getText();
				if (!emptyUrl(skinurl)) {
					previewImage.setUrl(getPreviewSkin(skinurl));
					return;
				}
				previewImage.setUrl(getDefaultSkin(name));
			}
		};
		preview.setAnchor(WidgetAnchor.CENTER_CENTER);
		preview.setX(background.getX() + 230).setY(background.getY() + 135);
		preview.setWidth(50).setHeight(20);
		preview.setPriority(RenderPriority.Low);
		preview.setText("Preview");
		
		this.setTransparent(false);
		this.setFixed(true);
		this.attachWidgets(plugin, background, previewImage, skinurllabel, skinUrlField, save, reset, preview);
	}
	
	private boolean emptyUrl(String url) {
		if (	url == null ||
				url.equals("") ||
				url.equals("http://") ||
				url.equals("https://") ||
				url.equals("ftp://")) {
			return true;
		} else {
			return false;
		}
	}
	
	private String getDefaultSkin(String name) {
		return getPreviewSkin("http://s3.amazonaws.com/MinecraftSkins/" + name + ".png");
	}
	
	private String getPreviewSkin(String url) {
		return previewUrl + "?url=" + url + "&.png";
	}
	
	private boolean setSkin(SpoutPlayer player, String skinurl) {
		PlayerConfigManager pcm = PlayerConfigManager.getManager(player.getName());
		if (emptyUrl(skinurl)) {
			pcm.set("custom-skin", null);
			player.resetSkin();
			return true;
		}
		pcm.set("custom-skin", skinurl);
		try {
			player.setSkin(skinurl);
		} catch (UnsupportedOperationException e) {
			String trySkin = skinurl + "?.png";
			if (skinurl.contains("?"))
				trySkin = skinurl + "&.png";
			try {
				player.setSkin(trySkin);
			} catch (UnsupportedOperationException e2) {
				sendNotification(player, "ERROR!", "Unable to load URL!", 377);
				return false;
			}
		}
		return true;
	}
	
	private void sendNotification(SpoutPlayer player, String label, String message, int materialId) {
		player.sendNotification(label, message, Material.getMaterial(materialId));
	}
}
