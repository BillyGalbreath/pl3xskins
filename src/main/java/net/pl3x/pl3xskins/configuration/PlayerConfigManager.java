package net.pl3x.pl3xskins.configuration;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import net.pl3x.pl3xskins.Pl3xSkins;

public class PlayerConfigManager extends YamlConfiguration {
	private final File dataFolder = ((Pl3xSkins)Bukkit.getPluginManager().getPlugin("Pl3xSkins")).getDataFolder();
	private static final Map<String, PlayerConfigManager> pcms = new HashMap<String, PlayerConfigManager>();
	
	public static PlayerConfigManager getManager(String s) {
		s = s.toLowerCase();
		synchronized (pcms) {
			if (pcms.containsKey(s)) return pcms.get(s);
			final PlayerConfigManager pcm = new PlayerConfigManager(s);
			pcms.put(s, pcm);
			return pcm;
		}
	}
	
	public static boolean isManagerCreated(String s) {
		synchronized (pcms) {
			return pcms.containsKey(s);
		}
	}
	
	public static void saveAllManagers() {
		synchronized (pcms) {
			for (PlayerConfigManager pcm : pcms.values()) pcm.forceSave();
		}
	}
	
	public static void removeAllManagers() {
		Collection<PlayerConfigManager> oldConfs = new ArrayList<PlayerConfigManager>();
		oldConfs.addAll(pcms.values());
		synchronized (pcms) {
			for (PlayerConfigManager pcm : oldConfs) pcm.discard(false);
		}
	}
	
	public static int managersCreated() {
		synchronized (pcms) {
			return pcms.size();
		}
	}
	
	public static Collection<PlayerConfigManager> getAllManagers() {
		synchronized (pcms) {
			return Collections.synchronizedCollection(pcms.values());
		}
	}
	
	private File pconfl = null;
	private final Object saveLock = new Object();
	private final String playerName;
	
	PlayerConfigManager(String p) {
		super();
		pconfl = new File(dataFolder + File.separator + "userdata" + File.separator + p.toLowerCase() + ".yml");
		try {
			load(pconfl);
		} catch (Exception ignored) {
		}
		playerName = p;
	}
	
	@SuppressWarnings("unused")
	private PlayerConfigManager() {
		playerName = "";
	}
	
	public boolean exists() {
		return pconfl.exists();
	}
	
	public boolean createFile() {
		try {
			return pconfl.createNewFile();
		} catch (IOException ignored) {
			return false;
		}
	}
	
	public void forceSave() {
		synchronized (saveLock) {
			try {
				save(pconfl);
			} catch (IOException ignored) {
			}
		}
	}
	
	public String getManagerPlayerName() {
		return playerName;
	}
	
	public void discard() {
		discard(false);
	}
	
	public void discard(boolean save) {
		if (save) forceSave();
		synchronized (pcms) {
			pcms.remove(playerName);
		}
	}
}
