package net.pl3x.pl3xskins.listeners;

import net.pl3x.pl3xskins.Pl3xSkins;
import net.pl3x.pl3xskins.guis.SkinSelectScreen;

import org.getspout.spoutapi.event.input.KeyBindingEvent;
import org.getspout.spoutapi.gui.ScreenType;
import org.getspout.spoutapi.keyboard.BindingExecutionDelegate;

public class KeyListener implements BindingExecutionDelegate {
	private Pl3xSkins plugin;
	
	public KeyListener(Pl3xSkins plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public void keyPressed(KeyBindingEvent event) {
		if (event.getScreenType() != ScreenType.GAME_SCREEN && !event.getScreenType().toString().equals("UNKNOWN")) {
			// we dont want to pop up while on any screen other than the main screen
			return;
		}
		event.getPlayer().getMainScreen().attachPopupScreen(new SkinSelectScreen(plugin, event.getPlayer()));
	}
	
	@Override
	public void keyReleased(KeyBindingEvent event) {
		// Auto-generated method stub
	}
}
