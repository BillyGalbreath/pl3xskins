package net.pl3x.pl3xskins.listeners;

import net.pl3x.pl3xskins.Pl3xSkins;
import net.pl3x.pl3xskins.configuration.PlayerConfigManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {
	private Pl3xSkins plugin;
	
	public PlayerListener(Pl3xSkins plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		plugin.loadAllPlayerSkinsCapes();
	}
	
	@EventHandler
	public void onPlayerChangedWorldEvent(PlayerChangedWorldEvent event) {
		plugin.loadAllPlayerSkinsCapes();
	}
	
	@EventHandler
	public void onPlayerQuitEvent(PlayerQuitEvent event) {
		PlayerConfigManager pcm = PlayerConfigManager.getManager(event.getPlayer().getName());
		pcm.discard(true);
	}
}
