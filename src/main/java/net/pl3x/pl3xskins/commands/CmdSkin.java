package net.pl3x.pl3xskins.commands;

import net.pl3x.pl3xskins.Pl3xSkins;
import net.pl3x.pl3xskins.guis.SkinSelectScreen;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

public class CmdSkin implements CommandExecutor {
	private Pl3xSkins plugin;
	
	public CmdSkin(final Pl3xSkins plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("skin")) {
			if (!cs.hasPermission("pl3xskins.command.skin")) {
				cs.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
				return true;
			}
			Player player = (Player) cs;
			SpoutPlayer splayer = (SpoutPlayer) player;
			splayer.getMainScreen().attachPopupScreen(new SkinSelectScreen(plugin, player));
			return true;
		}
		return false;
	}
}
