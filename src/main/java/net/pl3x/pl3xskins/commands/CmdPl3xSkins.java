package net.pl3x.pl3xskins.commands;

import net.pl3x.pl3xskins.Pl3xSkins;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdPl3xSkins implements CommandExecutor {
	private Pl3xSkins plugin;
	
	public CmdPl3xSkins(final Pl3xSkins plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("pl3xskins")) {
			if (!cs.hasPermission("pl3xskins.command.pl3xskins")) {
				cs.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
				return true;
			}
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reload")) {
					plugin.reloadConfig();
					plugin.loadConfig();
					cs.sendMessage(ChatColor.BLUE + "Pl3xSkins " + ChatColor.BLUE + "configuration reloaded.");
					return true;
				}
			}
			cs.sendMessage(ChatColor.BLUE + "Pl3xSkins " + ChatColor.GRAY + "v" + plugin.getDescription().getVersion() + ChatColor.BLUE + ".");
			return true;
		}
		return false;
	}
}
