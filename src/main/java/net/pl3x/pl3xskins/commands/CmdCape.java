package net.pl3x.pl3xskins.commands;

import net.pl3x.pl3xskins.Pl3xSkins;
import net.pl3x.pl3xskins.guis.CapeSelectScreen;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

public class CmdCape implements CommandExecutor {
	private Pl3xSkins plugin;
	
	public CmdCape(final Pl3xSkins plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("cape")) {
			if (!cs.hasPermission("pl3xskins.command.cape")) {
				cs.sendMessage(ChatColor.RED + "You do not have permission to use this command.");
				return true;
			}
			Player player = (Player) cs;
			SpoutPlayer splayer = (SpoutPlayer) player;
			splayer.getMainScreen().attachPopupScreen(new CapeSelectScreen(plugin, player));
			return true;
		}
		return false;
	}
}
